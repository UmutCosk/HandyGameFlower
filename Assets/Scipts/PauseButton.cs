﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseButton : MonoBehaviour
{

    public void PauseGame()
    {
        Time.timeScale = 0;
        GameObject.Find("Canvas").transform.GetChild(1).gameObject.SetActive(true);

    }
}
