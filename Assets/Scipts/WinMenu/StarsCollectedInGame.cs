﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StarsCollectedInGame : MonoBehaviour
{

    public void ShowStar()
    {
        CloseStars();
        for (int i = 0; i < EventManager.seedsCollected; i++)
        {
            this.transform.GetChild(i).GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Art/Stars/complete_star");
        }
    }

    public void CloseStars()
    {
        foreach (Transform child in this.transform)
        {
            child.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>("Art/Stars/complete_star_bg");
        }
    }
}
