﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MeasureTime : MonoBehaviour
{
    public float time;
    void Start()
    {
        StartMeasuring();
    }
    public void StartMeasuring()
    {
        time = 0;
    }
    float hundredTimer = 0;



    // Update is called once per frame
    void Update()
    {
        time += Time.smoothDeltaTime;

    }
}
