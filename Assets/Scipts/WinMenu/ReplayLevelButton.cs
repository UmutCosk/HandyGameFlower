﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReplayLevelButton : MonoBehaviour
{

    public void Replay()
    {
        EventManager.RestartLevel();
        GameObject.Find("ResetLine").GetComponent<ResetStage>().ResetThisStage();
        GameObject.Find("Canvas").transform.GetChild(1).gameObject.SetActive(false);
        //Unfreeze Constraints except Rotation
        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;
        EventManager.seedsCollected = 0;
        GameObject.Find("CollectedStars").GetComponent<StarsCollectedInGame>().CloseStars();

        //ResetTimer
        GameObject.Find("Time").GetComponent<MeasureTime>().StartMeasuring();

        ////If Unpause
        Time.timeScale = 1;
        GameObject.Find("Canvas").transform.GetChild(2).gameObject.SetActive(false);
        GameObject.Find("Canvas").transform.GetChild(3).gameObject.SetActive(true);


    }
}
