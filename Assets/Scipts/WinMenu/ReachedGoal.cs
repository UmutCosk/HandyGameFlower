﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReachedGoal : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Transform setRoot = GameObject.Find("Canvas").transform.parent;
            GameObject.Find("Player").transform.SetParent(setRoot);
            GameObject.Find("Player").GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeAll;

            //Unlock Next Level
            PlayerPrefSerialize.UnlockNextLevel(EventManager.currentLevel);

            EventManager.WinPanel();

            //GameObject.Find("Canvas").transform.GetChild(3).gameObject.SetActive(false);
        }

    }
}
