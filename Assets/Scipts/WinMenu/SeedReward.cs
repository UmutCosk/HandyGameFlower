﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SeedReward : MonoBehaviour
{
    Animator star1;
    Animator star2;
    Animator star3;

    private void Update()
    {
        //if (Input.GetKeyDown("q"))
        //{
        //    star1.SetBool("ToggleStar", true);
        //    Debug.Log("test");
        //}


    }
    private void OnEnable()
    {
        //Init Animation
        star1 = this.transform.GetChild(0).GetChild(0).GetComponent<Animator>();
        star2 = this.transform.GetChild(1).GetChild(0).GetComponent<Animator>();
        star3 = this.transform.GetChild(2).GetChild(0).GetComponent<Animator>();



        //Update Seed Collected
        PlayerPrefSerialize.UpdateSeeds(EventManager.seedsCollected);



        for (int i = 1; i <= EventManager.seedsCollected; i++)
        {
            switch (i)
            {
                case 1:
                    Invoke("DelayAfterTime1", 0.1f);
                    break;
                case 2:
                    Invoke("DelayAfterTime2", 0.3f);
                    break;
                case 3:
                    Invoke("DelayAfterTime3", .5f);
                    break;
            }

        }



    }



    void DelayAfterTime1()
    {

        star1.SetBool("ToggleStar1", true);
    }
    void DelayAfterTime2()
    {
        star2.SetBool("ToggleStar2", true);
    }
    void DelayAfterTime3()
    {
        star3.SetBool("ToggleStar3", true);
    }

    void OnDisable()
    {
        EventManager.seedsCollected = 0;
        Color color1 = this.transform.GetChild(0).GetChild(0).GetComponent<Image>().color;
        color1.a = 0;
        this.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = color1;
        //star1.SetBool("ToggleStar1", false);

        Color color2 = this.transform.GetChild(1).GetChild(0).GetComponent<Image>().color;
        color2.a = 0;
        this.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = color2;
        //   star2.SetBool("ToggleStar2", false);

        Color color3 = this.transform.GetChild(2).GetChild(0).GetComponent<Image>().color;
        color3.a = 0;
        this.transform.GetChild(2).GetChild(0).GetComponent<Image>().color = color3;
        //  star3.SetBool("ToggleStar3", false);
    }

}
