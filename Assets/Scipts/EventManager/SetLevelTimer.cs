﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SetLevelTimer : MonoBehaviour
{
    Animator timer;
    private void OnEnable()
    {
        this.GetComponent<TextMeshProUGUI>().text = (Mathf.Round(GameObject.Find("Time").GetComponent<MeasureTime>().time * 100f) / 100f).ToString();
        timer = this.GetComponent<Animator>();
        float delay = EventManager.seedsCollected * 0.2f;

        Invoke("ShowTimer", delay + 0.05f);
    }


    void ShowTimer()
    {

        timer.SetBool("ShowTimer", true);
    }

    private void OnDisable()
    {

        timer.SetBool("ShowTimer", false);
        this.transform.rotation = Quaternion.Euler(90, 0, 0);
    }
}
