﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartGame : MonoBehaviour
{

    private void OnEnable()
    {
        //  GameObject.Find("Canvas").transform.GetChild(0).gameObject.SetActive(true);
    }

    public void Restart()
    {
        Time.timeScale = 1;
        GameObject.Find("ResetLine").GetComponent<ResetStage>().ResetThisStage();
        GameObject.Find("Canvas").transform.GetChild(1).gameObject.SetActive(false);
    }
}
