﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ResetStage : MonoBehaviour
{
    GameObject Player;
    Vector3 offSetPlayer;

    private void OnEnable()
    {
        PlacePlayerOnThisPlattform();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        ResetThisStage();
    }


    public void ResetThisStage()
    {
        //Close collected Stars

        GameObject.Find("CollectedStars").GetComponent<StarsCollectedInGame>().CloseStars();
        foreach (Transform child in GameObject.Find("ResetableGameObjects").transform)
        {
            child.gameObject.SetActive(false);
        }

        foreach (Transform child in GameObject.Find("ResetableGameObjects").transform)
        {
            child.gameObject.SetActive(true);
        }
        PlacePlayerOnThisPlattform();
        GameObject.Find("Player").GetComponent<PlayerController>().currentState = PlayerState.Ground;
        EventManager.seedsCollected = 0;
        StartCoroutine("BlockInput");

        //Restart Dotween
        RestartResetablesDotween();

    }

    void RestartResetablesDotween()
    {
        DOTween.Restart("blackbar");
        DOTween.Pause("blackbar");
        DOTween.Play("blackbar");
    }

    public void PlacePlayerOnThisPlattform()
    {
        try
        {
            Player = GameObject.Find("Player").gameObject;
            offSetPlayer = new Vector3(0, 0.5f, 0);
            Player.transform.position = GameObject.FindGameObjectWithTag("Start").transform.position + offSetPlayer;
        }
        catch { }
    }

    IEnumerator BlockInput()
    {
        Player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        Player.GetComponent<PlayerController>().blockInput = true;
        yield return new WaitForSeconds(0.3f);
        Player.GetComponent<PlayerController>().blockInput = false;

    }
}
