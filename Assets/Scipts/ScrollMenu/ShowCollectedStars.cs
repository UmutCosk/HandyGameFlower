﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCollectedStars : MonoBehaviour
{

    private int[] tempArray;


    private void OnEnable()
    {
        SetTempArray();
        CloseStars();
        ShowStars();

    }

    private void ShowStars()
    {
        int counter = 0;
        foreach (Transform child in this.transform.GetChild(PlayerPrefSerialize.playerValues.currentStage - 1).transform.GetChild(1))
        {
            for (int i = 0; i < tempArray[counter]; i++)
            {
                child.transform.GetChild(1).GetChild(i).gameObject.SetActive(true);

            }
            counter++;
        }
    }

    private void CloseStars()
    {
        for (int levelstotal = 0; levelstotal < 2; levelstotal++)
        {


            foreach (Transform child in this.transform.GetChild(levelstotal).transform.GetChild(1))
            {
                for (int i = 0; i < 3; i++)
                {
                    child.transform.GetChild(1).GetChild(i).gameObject.SetActive(false);

                }
            }
        }
    }

    private void SetTempArray()
    {
        switch (PlayerPrefSerialize.playerValues.currentStage)
        {
            case 1:
                tempArray = PlayerPrefSerialize.playerValues.collectedStars1;
                break;
            case 2:
                tempArray = PlayerPrefSerialize.playerValues.collectedStars2;
                break;


        }
    }

}
