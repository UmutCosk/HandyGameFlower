﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OpenLevel : MonoBehaviour
{

    public int levelNumber;

    public void StartLevel()
    {
        PlayerPrefSerialize.SetCurrentLevel(this.levelNumber - 1);
        PlayerPrefSerialize.LoadPlayerValues();
        PlayerPrefSerialize.playerValues.returnedFromGame = false;
        PlayerPrefSerialize.SavePlayerValues();
        LoadScene();
    }

    void LoadScene()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        string sceneName = "Garden" + PlayerPrefSerialize.playerValues.currentStage;
        LO_LoadingScreen.prefabName = "Stock_Style";
        LO_LoadingScreen.LoadScene(sceneName);

    }

    private void Update()
    {

    }


}
