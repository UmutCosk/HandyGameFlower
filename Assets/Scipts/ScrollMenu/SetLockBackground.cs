﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetLockBackground : MonoBehaviour
{

    // Use this for initialization
    void OnEnable()
    {
        LockAllLevels();
        UnlockAllLevelsUntilLastWon();
    }


    void LockAllLevels()
    {
        foreach (Transform child in this.transform)
        {
            child.GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/StageLocks/stage_frame_lock");
            child.GetChild(0).GetChild(0).gameObject.SetActive(false);
            child.GetChild(0).GetComponent<Button>().interactable = false;
        }
    }

    void UnlockAllLevelsUntilLastWon()
    {
        int untilLevel = PlayerPrefSerialize.playerValues.stageLevelLockedUntil[PlayerPrefSerialize.playerValues.currentStage - 1];
        for (int i = 0; i <= untilLevel; i++)
        {
            this.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/StageLocks/stage_frame_default 1");
            this.transform.GetChild(i).GetChild(0).GetChild(0).gameObject.SetActive(true);
            this.transform.GetChild(i).GetChild(0).GetComponent<Button>().interactable = true;
            PlayerPrefSerialize.LoadPlayerValues();
            if (PlayerPrefSerialize.playerValues.collectedStars1[i] == 3)
            {
                this.transform.GetChild(i).GetChild(0).GetComponent<Image>().sprite = Resources.Load<Sprite>("Art/StageLocks/stage_frame_complete");
            }
        }
    }

}
