﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenStageMenu : MonoBehaviour
{
    int maxStages = 2;  // HIER ÄNDERN FALLS MEHR STAGES HINZUKOMMEN
    private void OnEnable()
    {
        PlayerPrefSerialize.LoadPlayerValues();

        if (PlayerPrefSerialize.playerValues.returnedFromGame)
        {
            CloseScrollPanel();
            OpenLevelList(PlayerPrefSerialize.playerValues.currentStage);
            PlayerPrefSerialize.playerValues.returnedFromGame = false;
            PlayerPrefSerialize.SavePlayerValues();
        }

    }

    public void OpenStage1()
    {

        PlayerPrefSerialize.SetCurrentStage(1);
        PlayerPrefSerialize.LoadPlayerValues();
        OpenLevelList(PlayerPrefSerialize.playerValues.currentStage);
        CloseScrollPanel();
    }

    public void OpenStage2()
    {
        PlayerPrefSerialize.SetCurrentStage(2);
        PlayerPrefSerialize.LoadPlayerValues();
        OpenLevelList(PlayerPrefSerialize.playerValues.currentStage);
        CloseScrollPanel();
    }


    public void OpenStage3()
    {

    }


    public void OpenStage4()
    {

    }

    public void OpenStage5()
    {

    }

    void CloseOtherLists(int exceptThisChild)
    {
        for (int i = 0; i < maxStages; i++) // HIER ÄNDERN FALLS MEHR STAGES HINZUKOMMEN
        {
            if (i != exceptThisChild)
            {
                GameObject.Find("LevelLists").transform.GetChild(i).gameObject.SetActive(false);
            }
        }
    }
    void OpenLevelList(int stageNumber)
    {
        PlayerPrefSerialize.LoadPlayerValues();
        GameObject.Find("Canvas").transform.GetChild(1).gameObject.SetActive(true);
        GameObject.Find("LevelLists").transform.GetChild(PlayerPrefSerialize.playerValues.currentStage - 1).gameObject.SetActive(true);
        GameObject.Find("LevelLists").transform.GetChild(GameObject.Find("LevelLists").transform.childCount - 1).gameObject.SetActive(true);
        CloseOtherLists(stageNumber - 1);
    }
    void CloseScrollPanel()
    {
        GameObject.Find("Canvas").transform.GetChild(0).gameObject.SetActive(false);
    }
}
