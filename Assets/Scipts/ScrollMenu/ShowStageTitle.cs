﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShowStageTitle : MonoBehaviour
{

    SnapScrolling snapScrolling;
    string[] titleNames = new string[] { "Indoor Garden", "Outdoor Garden" };


    void Start()
    {
        snapScrolling = GameObject.Find("Content").GetComponent<SnapScrolling>();
    }

    public void ChangeTitleName(int stageNumber)
    {
        GameObject.Find("TitleText").transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = titleNames[stageNumber];
    }
}
