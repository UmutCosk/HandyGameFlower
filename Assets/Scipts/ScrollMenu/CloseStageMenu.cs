﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CloseStageMenu : MonoBehaviour
{

    public void GoBack()
    {
        OpenScrollView();
        CloseWholeLevelLists();
        GameObject.Find("Canvas").transform.GetChild(1).gameObject.SetActive(false);
    }

    void CloseWholeLevelLists()
    {
        foreach (Transform child in GameObject.Find("LevelLists").transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    void OpenScrollView()
    {
        GameObject.Find("Canvas").transform.GetChild(0).gameObject.SetActive(true);

    }

}
