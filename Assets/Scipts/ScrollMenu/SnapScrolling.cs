﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SnapScrolling : MonoBehaviour
{
    [Range(1, 50)]
    [Header("Controllers")]
    public int panCount;
    [Header("Other Objects")]
    public GameObject panPrefab;

    private GameObject[] instPans;
    private Vector2[] pansPos;

    private Swipe SwipeControls;
    public int currentPosi;
    private int maxPosi = 1;
    public Transform Content;
    bool allowMovement;
    [Header("ScrollSpeed")]
    public float speed;

    //private void OnDisable()
    //{
    //    GameObject.Find("TitleText").gameObject.SetActive(false);

    //}

    //private void OnEnable()
    //{
    //    GameObject.Find("TitleText").gameObject.SetActive(true);
    //}

    void Start()
    {
        SwipeControls = GameObject.Find("Canvas").GetComponent<Swipe>();
        currentPosi = 0;
        speed = 10;
        instPans = new GameObject[panCount];
        pansPos = new Vector2[panCount];

        int childNumber = 0;
        foreach (Transform child in this.transform)
        {
            pansPos[childNumber] = -child.transform.GetComponent<RectTransform>().anchoredPosition;
            pansPos[childNumber].y = pansPos[childNumber].y - 100f;
            StartCoroutine(IgnoreLayout(child.gameObject));
            childNumber++;
        }
    }

    IEnumerator IgnoreLayout(GameObject child)
    {
        yield return new WaitForSeconds(0.02f);
        child.GetComponent<LayoutElement>().ignoreLayout = true;
    }

    public void ScrollLeft()
    {
        if (currentPosi - 1 >= 0)
        {
            currentPosi--;
            allowMovement = true;
        }

        GameObject.Find("TitleText").GetComponent<ShowStageTitle>().ChangeTitleName(currentPosi);


        GameObject.Find("DotContent").GetComponent<DotColorizer>().ColorizeSelectedDot(currentPosi);
    }

    public void ScrollRight()
    {
        if (currentPosi + 1 < panCount)
        {
            currentPosi++;
            allowMovement = true;
        }
        GameObject.Find("TitleText").GetComponent<ShowStageTitle>().ChangeTitleName(currentPosi);


        GameObject.Find("DotContent").GetComponent<DotColorizer>().ColorizeSelectedDot(currentPosi);
    }

    private void FixedUpdate()
    {
        if (allowMovement)
        {
            Content.GetComponent<RectTransform>().anchoredPosition = Vector2.Lerp(Content.GetComponent<RectTransform>().anchoredPosition, pansPos[currentPosi], Time.deltaTime * speed);
            float distance = Vector2.Distance(Content.GetComponent<RectTransform>().anchoredPosition, pansPos[currentPosi]);
            if (distance < 0.05f)
            {
                allowMovement = false;
            }
        }

    }

    private void Update()
    {
        if (SwipeControls.SwipeLeft)
        {
            ScrollLeft();

        }
        if (SwipeControls.SwipeRight)
        {
            ScrollRight();

        }
    }


}
