﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartGameSettings : MonoBehaviour
{

    public void SettingsWhenStartingTheGame()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        PlayerPrefSerialize.playerValues.returnedFromGame = false;
        PlayerPrefSerialize.ResetPlayerValues();
        PlayerPrefSerialize.SavePlayerValues();
    }
}
