﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GoBackToLevelList : MonoBehaviour
{

    public void GoToLevelList()
    {
        PlayerPrefSerialize.LoadPlayerValues();
        PlayerPrefSerialize.playerValues.returnedFromGame = true;
        PlayerPrefSerialize.SavePlayerValues();

        Time.timeScale = 1;
        EventManager.CloseAllLevels();
        SceneManager.LoadScene("ScrollMenu");
    }
}
