﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextLevel : MonoBehaviour
{

    public void GetNextLevel()
    {


        //Movement Reset
        EventManager.currentLevel++;
        PlayerPrefSerialize.SetCurrentLevel(EventManager.currentLevel);
        Debug.Log(EventManager.currentLevel);
        GameObject.Find("AllLevels").transform.GetChild(EventManager.currentLevel).gameObject.SetActive(true);
        GameObject.Find("ResetLine").GetComponent<ResetStage>().PlacePlayerOnThisPlattform();
        GameObject.Find("Canvas").transform.GetChild(0).gameObject.SetActive(false);
        EventManager.seedsCollected = 0;
        GameObject.Find("CollectedStars").GetComponent<StarsCollectedInGame>().CloseStars();
        //GameObject.Find("Canvas").transform.GetChild(3).gameObject.SetActive(true);

        //ResetTimer
        GameObject.Find("Time").GetComponent<MeasureTime>().StartMeasuring();

        //Unfreeze Constraints except Rotation
        GameObject.FindGameObjectWithTag("Player").GetComponent<Rigidbody2D>().constraints = RigidbodyConstraints2D.FreezeRotation;

        //Go to MainMenu if all levels in stage is finished
        if (EventManager.currentLevel == 21)
        {
            GameObject.Find("MainMenu").GetComponent<GoBackToLevelList>().GoToLevelList();
        }
    }


}
