﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DotColorizer : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    void SetAllColorsToWhite()
    {
        foreach (Transform child in this.transform)
        {
            child.GetComponent<Image>().color = Color.white;
        }
    }

    public void ColorizeSelectedDot(int dotNumber)
    {
        SetAllColorsToWhite();
        this.transform.GetChild(dotNumber).GetComponent<Image>().color = Color.black;
    }
}
