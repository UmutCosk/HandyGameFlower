﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour
{
    static public EventManager instance;
    [SerializeField]
    public static int currentLevel;
    PlayerController playerController;

    public static int seedsCollected;
    public static float levelTime;
    public static float timeCounter;

    private void Start()
    {
        seedsCollected = 0;
    }

    public bool testlevel;

    void OnEnable()
    {
        instance = this;

        if (!testlevel)
        {


            PlayerPrefSerialize.LoadPlayerValues();
            currentLevel = PlayerPrefSerialize.playerValues.currentLevel;
            RestartLevel();
        }
    }

    public static void RestartLevel()
    {

        CloseAllLevels();
        GameObject.Find("AllLevels").transform.GetChild(currentLevel).gameObject.SetActive(true);
        //GameObject.Find("ResetLine").GetComponent<ResetStage>().PlacePlayerOnThisPlattform();
    }

    public static void CloseAllLevels()
    {
        foreach (Transform child in GameObject.Find("AllLevels").transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    public static void WinPanel()
    {

        levelTime = timeCounter;
        GameObject.Find("Canvas").transform.GetChild(0).gameObject.SetActive(true);
        CloseAllLevels();
    }


    public static void ResetTimer()
    {
        timeCounter = 0;
    }

    private void Update()
    {
        timeCounter += Time.deltaTime;

    }



}
