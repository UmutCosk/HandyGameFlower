﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpPowerUp : MonoBehaviour
{


    PlayerController playerController;


    private void Start()
    {
        playerController = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
    }
    // Use this for initialization
    private void OnTriggerEnter2D(Collider2D collision)
    {
        playerController.currentState = PlayerState.Ground;
        Debug.Log(playerController.currentState);
        this.gameObject.SetActive(false);
    }
}
