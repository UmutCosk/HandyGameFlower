﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombPickUp : MonoBehaviour
{

    int maxCounter;

    private void OnEnable()
    {
        this.GetComponent<SpriteRenderer>().enabled = true;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        this.GetComponent<SpriteRenderer>().enabled = false;
        maxCounter = GameObject.FindGameObjectsWithTag("BombWall").Length;

        int counter = 0;
        foreach (GameObject child in GameObject.FindGameObjectsWithTag("BombWall"))
        {

            StartCoroutine(DelaySetActive(child, counter));
            counter++;
        }

    }

    IEnumerator DelaySetActive(GameObject child, int counter)
    {
        yield return new WaitForSeconds(counter * 0.05f);
        child.SetActive(false);

        if (maxCounter == counter + 1)
        {
            this.gameObject.SetActive(false);
        }
    }


}
