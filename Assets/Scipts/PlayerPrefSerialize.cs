﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine.UI;


[System.Serializable]
public class PlayerValues
{
    public int currentStage;
    public int currentLevel;
    public bool returnedFromGame;

    public int[] collectedStars1 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
    public int[] collectedStars2 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    public int[] stageLevelLockedUntil = new int[] { 0, 0 };

}




public static class PlayerPrefSerialize
{

    public static PlayerValues playerValues;
    private static string playerValuesKey = "PlayerValues";

    public static void LoadPlayerValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(PlayerValues));
        string text = PlayerPrefs.GetString(playerValuesKey);
        if (text.Length == 0)
        {
            playerValues = new PlayerValues();
        }
        else
        {
            using (var reader = new System.IO.StringReader(text))
            {
                playerValues = serializer.Deserialize(reader) as PlayerValues;
            }
        }
    }

    public static void SavePlayerValues()
    {
        XmlSerializer serializer = new XmlSerializer(typeof(PlayerValues));
        using (StringWriter sw = new StringWriter())
        {
            serializer.Serialize(sw, playerValues);
            PlayerPrefs.SetString(playerValuesKey, sw.ToString());
        }
    }

    public static void ResetPlayerValues()
    {
        playerValues.collectedStars1 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        playerValues.collectedStars2 = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        playerValues.stageLevelLockedUntil = new int[] { 0, 0 };
        PlayerPrefs.SetString(playerValuesKey, "");

    }

    public static void UpdateSeeds(int seedsCollected)
    {
        LoadPlayerValues();
        switch (playerValues.currentStage)
        {
            case 1:
                if (seedsCollected > playerValues.collectedStars1[playerValues.currentLevel])
                {
                    playerValues.collectedStars1[playerValues.currentLevel] = seedsCollected;
                }
                break;
            case 2:
                if (seedsCollected > playerValues.collectedStars2[playerValues.currentLevel])
                {
                    playerValues.collectedStars2[playerValues.currentLevel] = seedsCollected;
                }
                break;
            case 3:
                break;
        }
        SavePlayerValues();
    }


    public static void SetCurrentStage(int stageNumber)
    {
        LoadPlayerValues();
        playerValues.currentStage = stageNumber;
        SavePlayerValues();
    }
    public static void SetCurrentLevel(int levelNumber)
    {
        LoadPlayerValues();
        playerValues.currentLevel = levelNumber;
        SavePlayerValues();
    }
    public static void NextLevel()
    {
        LoadPlayerValues();
        playerValues.currentLevel++;
        SavePlayerValues();
    }
    public static void UnlockNextLevel(int currentLevel)
    {
        LoadPlayerValues();
        if (playerValues.stageLevelLockedUntil[playerValues.currentStage - 1] <= currentLevel)
        {
            playerValues.stageLevelLockedUntil[playerValues.currentStage - 1] = currentLevel + 1;
        }

        SavePlayerValues();
    }



}
