﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectedDetection : MonoBehaviour
{

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            EventManager.seedsCollected++;
            GameObject.Find("CollectedStars").GetComponent<StarsCollectedInGame>().ShowStar();
            this.gameObject.SetActive(false);
        }
    }
}
