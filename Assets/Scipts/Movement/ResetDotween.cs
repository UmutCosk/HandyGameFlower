﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class ResetDotween : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        DOTween.Init();
    }

    // Update is called once per frame
    //public void RestartLevel()
    //{
    //    DOTween.Restart("blackbar");
    //    DOTween.Pause("blackbar");
    //}
    //void OnEnable()
    //{
    //    DOTween.Play("blackbar");
    //}

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.enabled)
        {

            DOTween.Play("blackbar");
        }
    }
}
