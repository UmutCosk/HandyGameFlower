﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Swipe : MonoBehaviour
{

    private bool tap, swipeUp, swipeRight, swipeLeft;
    private bool isDraging = false;
    // Use this for initialization
    private Vector2 startTouch, swipeDelta;
    public bool Tap { get { return tap; } }
    public Vector2 SwipeDelta { get { return swipeDelta; } }
    public bool SwipeUp { get { return swipeUp; } }
    public bool SwipeRight { get { return swipeRight; } }
    public bool SwipeLeft { get { return swipeLeft; } }

    private void Update()
    {
        swipeUp = false;
        swipeRight = false;
        swipeLeft = false;

        #region Standalone Inputs
        if (Input.GetMouseButtonDown(0))
        {
            tap = true;
            isDraging = true;
            startTouch = Input.mousePosition;
        }
        else if (Input.GetMouseButtonUp(0))
        {
            isDraging = false;
            Reset();
        }
        #endregion

        #region Mobile Inputs
        if (Input.touches.Length != 0)
        {
            if (Input.touches[0].phase == TouchPhase.Began)
            {
                isDraging = true;

                startTouch = Input.touches[0].position;
            }
            else if (Input.touches[0].phase == TouchPhase.Ended || Input.touches[0].phase == TouchPhase.Canceled)
            {
                isDraging = false;

                Reset();
            }
        }
        #endregion

        // Calc the distance
        if (isDraging)
        {
            if (Input.touches.Length != 0)
            {
                swipeDelta = Input.touches[0].position - startTouch;

            }
            else if (Input.GetMouseButton(0))
            {
                swipeDelta = (Vector2)Input.mousePosition - startTouch;
            }
        }

        // Did we cross the deadzone?
        if (swipeDelta.magnitude > 100)
        {
            //Which direction?
            float y = swipeDelta.y;
            if (y > 0)
            {
                swipeUp = true;
            }
            //Which direction?
            float xr = swipeDelta.x;
            if (xr < 0)
            {
                swipeRight = true;
            }
            //Which direction?
            float xl = swipeDelta.x;
            if (xl > 0)
            {
                swipeLeft = true;
            }


            Reset();
        }
    }

    private void Reset()
    {
        startTouch = swipeDelta = Vector2.zero;
        isDraging = false;
    }


}
