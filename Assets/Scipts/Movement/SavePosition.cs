﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SavePosition : MonoBehaviour
{

    Vector3 saveStartPosi;

    private void Awake()
    {
        saveStartPosi = this.transform.position;
    }
    // Use this for initialization
    void OnEnable()
    {
        this.transform.position = saveStartPosi;
    }
}
