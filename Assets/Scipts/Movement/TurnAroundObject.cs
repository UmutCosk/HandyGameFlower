﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnAroundObject : MonoBehaviour
{
    [Range(1f, 500f)]
    public int speed;
    public Transform objectToTurnAround;



    // Update is called once per frame
    void Update()
    {
        this.transform.RotateAround(objectToTurnAround.position, new Vector3(0, 0, 1), speed * Time.deltaTime);
    }


}
