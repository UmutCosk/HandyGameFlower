﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackPlatform : MonoBehaviour
{
    PlayerController playerController;
    public bool playerIsStanding;

    private void Awake()
    {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        playerIsStanding = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.enabled)
        {
            playerController.currentState = PlayerState.Ground;
            playerController.gameObject.transform.parent = this.transform;
            playerIsStanding = true;
        }
    }


    private void OnCollisionExit2D(Collision2D collision)
    {
        if (playerIsStanding)
        {
            playerController.gameObject.transform.parent = null;
        }
    }
}
