﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayStanding : MonoBehaviour
{

    PlayerController playerController;
    public bool playerIsStanding;

    private void OnEnable()
    {
        playerIsStanding = false;
    }

    private void Awake()
    {
        playerController = GameObject.Find("Player").GetComponent<PlayerController>();
        playerIsStanding = false;
    }




}
