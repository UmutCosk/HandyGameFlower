﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public enum PlayerState { Ground, Air };
public class PlayerController : MonoBehaviour
{
    Rigidbody2D rb2;

    [Range(1, 100)]
    public float sideForce;
    [Range(1, 100)]
    public float initialJumpForce;
    [Range(1, 100)]
    public float downforce;

    //Controlls
    public Swipe SwipeControls;
    public PlayerState currentState;
    public bool blockInput;
    bool allowJumpAgain;
    float sideInput;
    float sideInputThreshold = 0.3f;

    void Awake()
    {
        //Init
        rb2 = this.GetComponent<Rigidbody2D>();
        currentState = PlayerState.Ground;
        //RigidBody Settings
        rb2.constraints = RigidbodyConstraints2D.FreezeRotation;
        rb2.drag = 4.5f;
        rb2.gravityScale = 0.20f;


        //Movement Forces
        sideForce = 100f;
        initialJumpForce = 11f;
        downforce = 25;
        allowJumpAgain = true;


        blockInput = false;


    }

    //Applying Force Move
    void Update()
    {

        if (!blockInput)

        {

            #region Movement

            #region Mobile Control
            //// Side Ways Movement
            //float sideInput = Input.acceleration.z;
            //if (Mathf.Abs(sideInput) > sideInputThreshold)
            //{
            //    if (sideInput < 0)
            //    {
            //        sideInput = -sideInputThreshold;
            //    }
            //    else
            //    {
            //        sideInput = sideInputThreshold;
            //    }
            //}
            //rb2.AddForce(new Vector3(sideInput * sideForce, 0, 0));

            //if (SwipeControls.SwipeUp && currentState == PlayerState.Ground)
            //{
            //    currentState = PlayerState.Air;
            //    rb2.velocity = new Vector2(rb2.velocity.x, 0);
            //    rb2.AddForce(new Vector3(0, 1 * initialJumpForce, 0), ForceMode2D.Impulse);
            //}


            //if (Input.touches[0].phase == TouchPhase.Stationary)
            //{
            //    rb2.AddForce(Vector2.down * downforce);
            //}
            #endregion

            #region PC Control
            //Side Ways Movement

            sideInput = Input.GetAxis("Horizontal");

            if (Mathf.Abs(sideInput) > sideInputThreshold)
            {
                if (sideInput < 0)
                {
                    sideInput = -sideInputThreshold;
                }
                else
                {
                    sideInput = sideInputThreshold;
                }
            }
            rb2.AddForce(new Vector3(sideInput * sideForce, 0, 0));

            if (Input.GetKey(KeyCode.UpArrow) && currentState == PlayerState.Ground && allowJumpAgain)
            {
                allowJumpAgain = false;
                currentState = PlayerState.Air;
                rb2.velocity = new Vector2(rb2.velocity.x, 0);
                rb2.AddForce(new Vector3(0, 1 * initialJumpForce, 0), ForceMode2D.Impulse);
                StartCoroutine(AllowJumpAgain());
            }


            if (Input.GetKey(KeyCode.DownArrow))
            {
                rb2.AddForce(Vector2.down * downforce);
            }
            #endregion

            #endregion

        }
    }

    IEnumerator AllowJumpAgain()
    {
        yield return new WaitForSeconds(0.25f);
        allowJumpAgain = true;
    }





}










